import React, { useState, useEffect } from "react";
import { View, Text } from "react-native";
import { postApi } from "../services/postService";
import { PostItem } from "./PostItem";

export default function PostContainer2() {
    const [limit, setLimit] = useState(3)
    const { data: posts, error, isLoading } = postApi.useFetchAllPostsQuery(limit);

    // useEffect(() => {
    //     setTimeout(()=> {
    //         setLimit(2)
    //     }, 2000)
    // }, [])

    console.log(posts, '3');
    
    
    return (
        <View>
            {isLoading && <Text>Loading</Text>}
            {error && <Text>ERROR...</Text>}
            {posts?.map((post, id) => (
                <PostItem key={id} post={post} />
            ))}
        </View>
    );
}
