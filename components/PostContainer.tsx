import React from "react";
import { View, Text } from "react-native";
import { postApi } from "../services/postService";
import { PostItem } from "./PostItem";

export default function PostContainer() {
    const { data: posts, error, isLoading } = postApi.useFetchAllPostsQuery(3);

    
    
    return (
        <View>
            {isLoading && <Text>Loading</Text>}
            {error && <Text>ERROR...</Text>}
            {posts?.map((post, id) => (
                <PostItem key={id} post={post} />
            ))}
        </View>
    );
}
