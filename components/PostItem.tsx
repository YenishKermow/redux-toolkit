import React, { FC } from "react";
import { View, Text } from "react-native";
import { IPost } from "../models/IPost";

interface PostItemProps {
    post: IPost;
}

export const PostItem: FC<PostItemProps> = ({ post }) => {
    return (
        <View>
            <Text>{post.id} {post.title}</Text>
        </View>
    );
};
