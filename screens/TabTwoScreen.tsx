import React, { useState, useEffect } from "react";
import { StyleSheet, Button } from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";

import EditScreenInfo from "../components/EditScreenInfo";
import PostContainer2 from "../components/PostContainer2";
import { Text, View } from "../components/Themed";

export default function TabTwoScreen() {
    const [hasPermission, setHasPermission]: any = useState(null);
    const [scanned, setScanned] = useState(true);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === "granted");
        })();
    }, []);

    const handleBarCodeScanned = ({ type, data }: any) => {
        console.log(type, data);

        setScanned(true);
        alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    };

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View style={styles.container}>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            />
            {scanned && (
                <Button
                    title={"Tap to Scan Again"}
                    onPress={() => setScanned(false)}
                />
            )}
        </View>
    );
    // return (
    //   <View style={styles.container}>
    //     <PostContainer2 />
    //   </View>
    // );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: "80%",
    },
});
