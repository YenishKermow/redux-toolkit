import React, { useState, useEffect } from "react";
import { Button, StyleSheet, View, TextInput } from "react-native";

import { RootTabScreenProps } from "../types";

export default function TabOneScreen({
    navigation,
}: RootTabScreenProps<"TabOne">) {
    const [text, onChangeText] = React.useState("");

    return (
        <View>
            <TextInput
                style={styles.input}
                onChangeText={onChangeText}
                value={text}
                placeholder='Write something...'
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    title: {
        fontSize: 20,
        fontWeight: "bold",
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: "80%",
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
});
